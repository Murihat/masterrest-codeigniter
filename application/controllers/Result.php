<?php
use Restserver\Libraries\REST_Controller;
defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . 'libraries/REST_Controller.php';
require APPPATH . 'libraries/Format.php';

class Result extends REST_Controller {
	
    function __construct()
    {
        parent::__construct();
        $this->load->database();

        // Configure limits on our controller methods
        // Ensure you have created the 'limits' table and enabled 'limits' within application/config/rest.php
        //$this->methods['users_get']['limit'] = 500; // 500 requests per hour per user/key
        //$this->methods['users_post']['limit'] = 100; // 100 requests per hour per user/key
        //$this->methods['users_delete']['limit'] = 50; // 50 requests per hour per user/key
    }

    public function printSuccess($data){
        return $this->response([
            'status' => 0,
            'message' => "SUCCESS",
            'data' => $data
        ], REST_Controller::HTTP_OK);     
    }

    public function printError($data){
        return $this->response([
            'status' => 1,
            'message' => "ERROR",
            'data' => $data
        ], REST_Controller::HTTP_OK);     
    }

    public function customError($status, $message, $data){
        return $this->response([
            'status' => $status,
            'message' => $message,
            'data' => $data
        ], REST_Controller::HTTP_OK);    
    }


}
