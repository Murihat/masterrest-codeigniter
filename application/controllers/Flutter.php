<?php
use Restserver\Libraries\REST_Controller;
defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . 'controllers/Result.php';

class Flutter extends Result {

    function __construct()
    {
        parent::__construct();
        $this->load->helper('url');
        $this->load->model(array('m_member'));
    }


    private function uploadPhoto($member_id, $strOldFile = '', $isDeleteOldFile = false)
  {
      $folder = 'assets/imgs';
      if (!file_exists($folder)) {
          mkdir($folder, 0777);
      }
      $folder = 'assets/imgs/' . $member_id;
      if (!file_exists($folder)) {
          mkdir($folder, 0777);
      }

      // $folder = 'assets/imgs/' . $member_id;
      // if (!file_exists($folder)) {
      //     mkdir($folder, 0777);
      // }

      if ($isDeleteOldFile && $strOldFile != "") {
          @unlink($folder . $strOldFile);
      }
      if (isset($_FILES['photo']['name']))
      {
          $strNewFile = $folder . basename($_FILES['photo']['name']);
          move_uploaded_file($_FILES['photo']['tmp_name'], $strNewFile);
          $checkData = file_get_contents($strNewFile);
          if (stripos($checkData, "<?php") !== false)
          {
            return "";
          }
      }else{
          $strNewFile = $folder;
      }

      return $strNewFile;
    }

    public function member_get()
    {
        $member_id = $this->get('member_id');

        if($member_id){ //get member by member_id
            $member    = $this->m_member->getSingleMember($member_id);
        }else{ //get all member if not have params member_id
            $member    = $this->m_member->getAllMember();
        }

        if($member){
           $response = $this->printSuccess($member);
        }else{
           $response = $this->printError($member);
        }  

        return $response;
    }


    public function member_post()
    {
        $member_id = $this->post('member_id');
        $member_foto_url= null;
        $member_foto = null;

        if(!empty($member_id)){
            $photo      = $this->post("base64image");

            $member_foto_url = $this->uploadPhoto($this->post('member_id'));

            $count_photo = 0;
            if(!empty($photo)){
                $ext            = '.png';
                $filename       = time()."_".str_replace(array('.',' '), '_', $this->post('member_id'));
                $member_foto      = $filename.$ext;
                
                $data_decode    = base64_decode($photo);
                $img_file       = @fopen($member_foto_url."/".$member_foto,'w');
                if($img_file)
                {
                    @fwrite($img_file,$data_decode);
                    @fclose($img_file);
                }
                if (file_exists($member_foto_url.$member_foto))
                {
                    $count_photo++;
                }
            }

           $member_foto_url = base_url().$member_foto_url."/";;
        }
        

        $data = [
            'member_name' => $this->post('member_name'),
            'member_email' => $this->post('member_email'),
            'member_password' => md5($this->post('member_password')),
            'member_foto_url' => $member_foto_url,
            'member_foto'   => $member_foto,
            'member_address' => $this->post('member_address'),
            'member_phone'  => $this->post('member_phone'),
            'member_status' => 1,
            'member_verification' => 1,
            'member_jenkel' => $this->post('member_jenkel'),
            'member_birthday' => $this->post('member_birthday')
        ];


        $checkMember = $this->m_member->checkMemberByEmail($data['member_email']);

        if($checkMember){
            $response = $this->customError(1, "Email Telah Terdaftar", array());
        }else{
            $result = $this->m_member->insertMember($data);

            if($result){
                $response = $this->printSuccess(array());
            }else{
                $response = $this->printError(array());
            }
        } 

       return $response;
    }

    public function member_put(){
        $member_id = $this->put('member_id');
        $photo = $this->put('base64image');
        $member_foto_url= null;
        $member_foto = null;

        if(!empty($photo)){
            $member_foto_url = $this->uploadPhoto($member_id);
            $count_photo = 0;
            if(!empty($photo)){
                $ext            = '.png';
                $filename       = time()."_".str_replace(array('.',' '), '_', $member_id);
                $member_foto      = $filename.$ext;
                
                $data_decode    = base64_decode($photo);
                $img_file       = @fopen($member_foto_url."/".$member_foto,'w');
                if($img_file)
                {
                    @fwrite($img_file,$data_decode);
                    @fclose($img_file);
                }
                if (file_exists($member_foto_url.$member_foto))
                {
                    $count_photo++;
                }
            }

            $member_foto_url = base_url().$member_foto_url."/";

            $data = [
                'member_id'         => $member_id,
                'member_name'       => $this->put('member_name'),
                'member_email'      => $this->put('member_email'),
                'member_foto_url'   => $member_foto_url,
                'member_foto'       => $member_foto,
                'member_address'    => $this->put('member_address'),
                'member_phone'      => $this->put('member_phone'),
                'member_jenkel'     => $this->put('member_jenkel')
            ];
        } else{
            $data = [
                'member_id'         => $member_id,
                'member_name'       => $this->put('member_name'),
                'member_email'      => $this->put('member_email'),
                'member_address'    => $this->put('member_address'),
                'member_phone'      => $this->put('member_phone'),
                'member_jenkel'     => $this->put('member_jenkel')
            ];
        }


        $where = ['member_id' => $member_id];

        $result = $this->m_member->updateMember($data, $where);

        if($result){
            $response = $this->printSuccess(array());
        }else{
            $response = $this->printError(array());
        }

        return $response;

    }



    public function member_delete()
    {
        $member_id = (int) $this->delete('member_id');

        $where = ['member_id' => $member_id];

        $result = $this->m_member->deleteMember($where);

         if($result){
            $response = $this->printSuccess(array());
        }else{
            $response = $this->printError(array());
        }        
    }

}



// data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAQAAAAEACAYAAABccqhmAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAyZpVFh0WE1MOmNvbS5hZG9iZS54bXAAAAAAADw/eHBhY2tldCBiZWdpbj0i77u/IiBpZD0iVzVNME1wQ2VoaUh6cmVTek5UY3prYzlkIj8+IDx4OnhtcG1ldGEgeG1sbnM6eD0iYWRvYmU6bnM6bWV0YS8iIHg6eG1wdGs9IkFkb2JlIFhNUCBDb3JlIDUuNi1jMDY3IDc5LjE1Nzc0NywgMjAxNS8wMy8zMC0yMzo0MDo0MiAgICAgICAgIj4gPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4gPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIgeG1sbnM6eG1wPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvIiB4bWxuczp4bXBNTT0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL21tLyIgeG1sbnM6c3RSZWY9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9zVHlwZS9SZXNvdXJjZVJlZiMiIHhtcDpDcmVhdG9yVG9vbD0iQWRvYmUgUGhvdG9zaG9wIENDIDIwMTUgKFdpbmRvd3MpIiB4bXBNTTpJbnN0YW5jZUlEPSJ4bXAuaWlkOkI4MzI2NDU5RERBMTExRTlCNENERDBBQTA4RjBBMzg1IiB4bXBNTTpEb2N1bWVudElEPSJ4bXAuZGlkOkI4MzI2NDVBRERBMTExRTlCNENERDBBQTA4RjBBMzg1Ij4gPHhtcE1NOkRlcml2ZWRGcm9tIHN0UmVmOmluc3RhbmNlSUQ9InhtcC5paWQ6QjgzMjY0NTdEREExMTFFOUI0Q0REMEFBMDhGMEEzODUiIHN0UmVmOmRvY3VtZW50SUQ9InhtcC5kaWQ6QjgzMjY0NThEREExMTFFOUI0Q0REMEFBMDhGMEEzODUiLz4gPC9yZGY6RGVzY3JpcHRpb24+IDwvcmRmOlJERj4gPC94OnhtcG1ldGE+IDw/eHBhY2tldCBlbmQ9InIiPz6Xc046AAAaW0lEQVR42uydC3Bc1XnHv7sPrVYryXpYtmXJlmUJv3jEBLCJgWIbbAwEgk0CyVBKJqSEaUJLnp2WtHTSkJmQUNrJo7RM27SBMFBsHg00hNbYCSXYPGwcHraxJdnW03qvtFqt9tX73dWalax933vPffx/MzvWWKt7zz3nfP/zfed851xp9K4WAqYmLvj+EprAvLhQBTByDcsGcYAAAAsZuxrPAlGAAACLGnwhzwtBgADA4FEfEAQIAIwezKwriAEEAEaPOoQYQABg9KhbiAEEAEYPIAYQABg+mN0OEAIIAAwf7QIhgADA6NFWEAMIAAwf7QchgADA8AGEAAIAwwcQgtk4UAUwfrQ1PAB0BgBvAAIAwwcQAggADB9ACDAHAOMHGCDgAaBRAbwBeAAwfgBvAB4AGg/AG4AHAOMH8AYgAGgoABFACIDGAQgJ4AHA+AG8AQgAGgJABBACoPIBQgJ4ADB+AG8AAgDjBxABCACMH0AEIAAwfgARgADA+AFEAAIA4wcQAQgAjB9ABCAAMH4AEbC5AMD4AUTApgIA4wcQAZsKAIwfQARsKgAwfgARsKkAwPgBRMCmAgDjBxABmwoAjB9ABGwqADB+AATbggPGD4B9RcAB4wfAviLgQJ0DYF/0PhMQo79GROSm7KVaGoh4qSvopEA4RqfHwxSOxig4FT7zPafDQT6Pm8pKnFReIv/sdlCtJ0YVrijVOoNUTX7yxEOalrVXqqPDwUpqG4nQbfW9mt/PhF6AZEUBgPGrSFAqoyPhBXTUL1H74ARFYzHa1V0+y7nzTP/szcER5I+bdiz+SCjqKkqputQpfySqkgVivitIFVKQfPEJueNEcirjYLzijCi1D4fIH0wY+67uRPdbUtFIW3zH0aCCRMAF4zcPo1IlHQjW0YGeoDKq7+p2Tv+mXLV77OquTPMb5/R9ymWR8OdxvdSyeVJEKcHB3gBtaUHbihIBF+rZ+ByMLKNXuyPK6JkYOb05jOrakV4k8icciVJI8iAMEIQ0epfm8ovRv8CY/pVAE73ZNUbxeFxVozMaz24ap0bqQ6OnsVEzewAw/gIM/6WxJnqn2z892ldY/plDcZdFX75t/FDABeM3DnsmW+i1E0nDr7TNc0/GnRAAQSKAOQAD8F5sKf3yeIiiMb+lXf10lEpRdAJBaCUAGP1zgJfJnuipo97RCVsafhKfYwo9RpAX4ILxi+H1qWba3T4uG77LVu7+bHhJ0UtYARAlAggBBPCvvU3yqD9u61E/lXlxPyrBIiEARv8MdNJCeuK4ROHIBIx/Gs44BOK8ABeMX2+XH4afSnNtGSpBoAggBNCBZ0aW0wd9YzD+WdzcMEYXVbtRERYIATD6p+Hn/cvo1BCMf85OE49Ti9SFihDoBSAA05BHOpfIxh+A8adBkiRlQhSIQw0BwOg/Bz8+2UhDgUkYfwZ2dlUoORAPty9WdjoC/W0PcwAaGX9i5x46dTaSdcTbmz91zlI613ESlWKiOQCM/nO4/f4gRv7ChMBPHYtb6PoKHBCi11yAC8avHhzPDgWwxl+sCAzVLKPb6zpQITqIACYBVeKpoWbb5/SrJQI8ccphFNCeQgUAo38KvI33WD+SfNQUAZ5D4clBPi0IaGeTmAQsEk7vTezhh/GrHw4QRWLV9JVWP3njE6gUDShEADD6p8C5/TB+7XjypE/59yutBBHQYC4AHkARcJZfOBJARegkAn/aGsXhoYLnADD6T3M83oAsP51F4KdttagIlW0UqwCFxqhtURi/znCyEOdZAHEeAKDE1l4+zx7oLLqy4HJ6NYdeQH8BgPs/zd4TWO8XKQIcevHSKyjeVuEB5Al3PH4PHxArArz0ykuwQB8PAKP/NPtOIeHHKCLAS7CgOJuFB5AHfH4/Rn/jwPMwnIINtPUAMPpP80pnDKO/wbwATsHmJVlQmO3CA8gRPrAi+W57YCwReL4DY5TWcwC250CwDqO/QeH8AKwKaCMAkNZp3j2N0d/IXgCvCoD8bRgeQC4jjFQG998EvByAF4AQQAOOhBfA/TeBF/BGJ7wANQUA7v807ePQSbPAadogd1tGz86BY4PYh24WL+B3XQjVEAKoDDb+mAdeEUCKcPECAPd/ml6pDpVgMi/gnbFyVESONg0PIAuDUS8mAE3Gu73jqASEAOrQP4W315oN3q8Bz61wAYD7n0IgjDowYxjQEUIYkIttwwPIwtAkJgDV4Or1S8gh6bd99/0h7NpECKCGBxCCABRLwwIfPfT1y+kn911JdVWlutyT39IEIABFEwxDAIrlc9euIKdToisuXExPPLiNzm2p0aftpDJUfp4CgPh/FhEcAFIUZaUu2rH5oxz9RbVl9B8PbKHtm5drPg/QHatGA2SZB4AHkAUkARXHnTetocrykhn/53E76W+/fCndf/c6cru06YI839AfLkEDIAQAouB4//YbVqX9/We2tNK/fedqTeYFYvE4jYXRvSEAQBh//oWLlBAgE2tXzqedf3cdfeJ89dN3h0MI3/IRAMT/QDV42W/bZU05fbdmXik98leblXBBTYYmImiILPMA8ACA6rE3j/p//aVL8vo7XiX46u1r6Ydfuyyr15ArkRjGNIQAQFc49n7oG5cro3ohsNfw5IPbqLmh+P0X4ShCAAgA0JV7PnuBst5fDGz8LAIcRhQDbw0GEACgE5suaaQv7jhXlWtxGPD337yC7r1tra4pxHYVAARLoCg+tmI+ff/eDUosrxrypb64Yw09ev8m3VKIbUQcHgBQhRVNVfSP921UbfJuNuvPX6RrCjFCAJAz37jjQtuP/D/7ztVnZfupDacQP/a9rZqnEEMAQF58/sbVSpxqRzh559H7N2tu/Ek4bVjrFGIIAMgbjlO5U9ppsopH4h9r6PZnglOIH5e9Ad5mDCAAhoA7pR0mq1jk/uILFykjMW/qEcWalhr6zx9ci44HATAOyckqjoutCI+4vJX3tutXGqI8eoUeVhcALAGqiLLf/btblHkBK8Wp7PI/8/D1yuYdYBniLtSB+vBaOM8LXLW+kb736Bv0u9/3mfZZOCvv2398seLdAOsBAdDYeB79m6votwe66eGfH6SjJ0ZMU3ae3Lvr5vPojhtXYcYdAgCKgXPjN1xQTy+/fpL+eed7hhYCNvzPbVtBd25fgxgbAgDUDAt4p9u2DU20791eeuyFo7T3zS5l95wR4NWLz8qG/5mtrQXv5AMQAJANKbFawJ/ewQn65d4OevHVDiFeAS/prT9vAd28pZW2XLpU3Tx+AAEAmeEVA54s5E97l5/+d18n7TvUQ29+0E/hiDZ72Tmev3h1HW2VvZGr1jVitIcAACPAE4ZJMZiYjNDvPxygg4cH6IP2YfkzRD39EwWFC7x2v7q5Rv5U09pV8+VPndAEHgABAFngibhkmJAkFI5S9+kAdZ4ep8BEWAkfQqGoIhZMha+EXC6JfF43LZpfRo0Lymmh7GGISNUFEACgMjxqs5fQvLjyzFwCABAAuwHDByqCDA8A4AGAXOC18hvWL6Z1K2toeX05VZa5USmCef0ftlDv8CT1DgVp/5Eh+q993dQ/MomKgQCoa/hf3b6Crl3XQA4Je6eMhNfjpOZFPuXziTXz6Z6bVtJ/7++ih585Kv/WjwqCABTHisZy+tZtVygdDRsnTRDTygJ9veylbV67kJ59/CX5f/pRKRmQRu9qQa9Ow4m6DbT8Dy4ghyv9urmzDgdViiTaP5T2d7FIlNp+c4ia+l9DRcEDyI++8hVZjV9LOBPwqZc+pBdfPUEfnkykCZ+ztIpu2rycbtq03HA79Li8z77SRs/ubptR3usub6JbrjlHSHm57bgNu18aoIXjR9Gp4QHkTuTGL5HXm10ftfAAOMnnTx7Yk3Z/AB/D/dP7NiqpxEZAZHkzeQBJgsEIuZ7/J3TquUQSVTCH67/mFvKWiFlw54y/TMbE8O/+7Pu/Ub4rGi4DlyVbefmZRJWX25LbFEAAcmL56hrevyvk3k//+lhOOwPfOz6kfFc0XAYuSzb4mYSVV25LpU0BBCAb7fWbyCEw245jfi2+a/fycpty2wIIQEZK6huFjf7M+21DmnzX9uWV21RpWwAByER1ndc0ZTXbWX2iy2umtoUACKKiXGyVrFmee6zKy2yiMVN5RbctBABkhdfNtfguygsgADkwNh4Tev9Pb21V9v1ng9fW+bui4TJwWbLBzyS6vKLbFgJgAob7g0Lvzwd/8Bt3MxlVMrHGCEd7cRm4LNnKy88kuryi29aIIBNwFrxUtOKylTmvBGiyF0BukVAkqqybz04FFplam4l0qctcXh75tTL+XDIBE1+M0tH/O0LNPa+gk0MAMuP+9N1iBQBoIgDhpx9BhSEEyM6x94eUDgOsohLRRJsCCEAuNL3/FAWn4BhZBW5LblMAAciZvr2vwwuwyOivtCVIKwA4Z3YOGkbfoWN7D0EEzO76y23IbQnmRIIHkCkU6H+NOn+1W9lPDiEwl+Fzm3Hb4TSgLAowelcL/4uANwu8n7x1zfSMf8oKAVYBBNt66iqAbPgxuSe3fTCEmD9H+4cA5AnnCfCuMt5Ywrnlrqoykrw+VIwA4sEARUYmlAw/TvKZ6unEOn+eAoAzAfNE6WA90wOO/HHd+TVUikgPYOcjxAeNlaEqCgJzAABAAAAAdhYALAUC87n/EUxdFRP/wwNQgVAQW0yF1X0IdY8QQDBTIeQHCEEe/KNhCAAEQDATY2FUgiAHFnUPARBOcGQclYC6Ny2uWZMCmFXJk/DomKnKm23/vJkyG81W98byn+ABqFOTI+Z6/bTk8RT0O9Q9QgAwBxWjHUpKqmkavNxb0O+MBtc51z2AAAilarLbZC3uIGdt1YzRnn/m/+PfmUcBTFj3Bp8DwDxAgfSdmqBFK3ymEgFHJZfXvJuY+jonqApdr6j4Hx6ASvhP9KASUOcIAeyKp+dDU80DmB2ua65zAAEwBAv97yMvXUe4rrnOgTYCgI1BBXDqAyxJoa7NFf/DA1BTSQ+/jTBAJ/ef6xogBDBcGDAR0H9jUDwcoXDXCIVPDlNsLKT5/fgefC++J99bb7iO4f5rLwAIAwqg841junsB0cEJik9FKR6NUWRgnOIh7TbI8LWVe8j34nvyvfWm60AHOppK7j88AJVpOv6C8MnAyIAsQDENtsnGYolri3y2sXFqOPoiOhpCAOPS8Y6+2WnOeTPz93lkjpweVzedS74WX5OvPePe1V7d69YZxxZgvQQAYUAB1L+zSxmpdPPrvB5yVpTMHKyDsqve51fHE+CRX74WXzMVlyw8kset6+jPdQvUc//hAWgxIssjVPtbp3SdC3DWVpBU4jxLBMI9/qLmBPhv+Rqzjd/hdZOjuly35+O6bHu9DaM/QgBzsOTdp/U9K1DWd3d95VkiwC57uNtP0YGxvGbs+bv8N/y3s91+vodrQbmu/iHX5dIjz6FjCRAAhAEFcnLvAX1XBBwORQR4dJ5NdGyKwp2jFOmVR3N/IOEVcHgQT8T3/DP/H/+Ov8Pf5b856xbytfkeeu4a5DrkugTqu/9KKIf60YalXXup79RqfXcJyobpWlhJseFxioyenRPArnzCnZ/M+9Ic8ytuv85DAu+05LoECAFMR8XL/6LrhGBS7x015eRefHZIUNDl5Gvwtfiaehs/113p7sfRkQQLAMKAAlEmBH+1T0iKMM/QuxuqyL2ofM6wIGvHYHdf/lu+hp6z/amuf9v/vE2lEZz7p5X7jxBAB5b0vUbH31pCLRc1CHmLMC8TuuQPx/mxQFiO9acoxu8yiMaVjD7lO055HHBK5PA4ZWMvIYfPLfR0oPhEgDrePQ3XX4/+Mf168JzaBdVVOCNb76aFS/Aq8VzoPdpPVb9+BBWh8eiPOQAd4Q49NoJ17Gz4e0aUuRNgnDkAzAWoRPj5xygw4EdFpGFyaEypIyT86DP6wwPQGV94mAK7nlA6Ojjb+P1P/0KpI2BMDwCoQOXUaaWjwxOYafwju55S6gbo7CrkMQmYBJOBanR6VwU5PvV5qqhy23ZikJf6eF4k9tzPsNwnwP2HByAQ7vAlO3+kZLrZ8SgxfmZ+dq4DGL+5QgBMBqoIrw4cf6vLViLAz8rPjKU+saN/oSEAwgANOLVwAzVvW0+uinJLPyen93J2JCdIAfMKAERAo3mByc23WTJhKOnyc24/XH5jGD8EwKCcbLiSll/9cXK6JNMLARs+7+fnLb1I7bWWAEAENKZz7a3C9hCoGes3HnwSjWlA41dDACACOoQFA+ddR61XnksUNU+GXPv+dqo88AISewxs/AyWAQ0Ox8tVh39L7hU7yFlzAZHTbdzCymVz+JaQu+UGqji0G8ZvAtTYDizBC1CfMLnlSk3os7RmI0muUnI2XEjOxWspNtRG0cFDFA8ZI5tQ8lSSY94qctQ2K+VMlnnq7Z3THSQmPw3y+402+qslAEADo2eu+eYG5d+H6rdTfarbVtuifCJtuyk6cCxxYIfe+/enzxF0zm8l1/LNZ/36yB9up69f1aP8/NIPXqMp8kAMLOoBwAsoAjZ4NvzZRp/EN7FoVk1LKX9bQrHxABGfOiYLgOR2Kwd6SG6n+oLABh+OKgeKxMPhM+8ccMwvSfsnXPZAWe+MZ0oVAxYCFgQgZvSHByCQKDnlj2tOo0+lUWqkjb4LcxyRQ8onYZkJQVB6ituVEASHlDj9R3LM7D7K6cAJQ1ROCYrFpw0+cZR4qsHnCpe5UdpPR6h3xv/PFgNl6oAiSm0A83oA8AIKcPMzGX6ShpLaIlz0hBicEQWd4bIfyfD75POzELAYIjzQd/TXwgOACKhk+ElaymtM+8xc9t3B7N9LFQIODyAE+hg/QgADG36S+jLzZgIqZQ/m/n0IgblDAHgBM8Lqjyb3CjH8JPNKSkxbB4WWfbYQYLJQm9FfSw/A1iKQnOUuxvBzweER7x1kKkMoUpzRpgoBU0IhGL/a7QdRVQ+e2Vfb+EenptK4GLK+TifdiPUh05dhLDylyi2Sdcl1y3UMzDEHYCsvQKtRv2ciQFQ9V+1KJJVWiR+WMpRhIBRU7T6zVwxs5g1odgiPw6wFt/Kon8rx8aH0jVe5iBf5BXZLd6IMBZQd3oAxbMil0wNY0hPQI9bvmhrMULMSOWsaKDrYIeT5+d6pmYlzlt1LmomADbwBzQdQzAEY2PiZzngn7QkcSG+Ei1YJq4NM9+Yyc9m1JNUbAMYWAMuEAry8p5fxM5xLn9EIF6yS43ABLx2V78n3LqbsaotA3FrjmS4249L5gUwdCiTz9/Uw/FT2n+6ljc3pwwB30yU0dWSPrmXie2Zy/7nMepEaElhkX4FuA6bDqg+mNpzUI8L4md3+Q5nDgMVryVG5UD+3Ub4X3zOT+89l1htuG26j1N2VMH7MAahi/OxeijB+pt93LHNSjTwSl6zZRpJT+6xBvgffK9Poz2XlMouA22j2FmtgLAEwlReQjC1FGX+Sfz/5ZkYvQCqroZLztmncWxzKPfhemUZ/LqtIkiJgwslByQ4CYBoR0HOyLxtHvG+mzwpMNmZtC3k+dqM2pwNJbvKc/0nlHpngMnJZRWPCFQIhNuGw2wPn4/YbxfiTPHry1YxewBkRuPAWVVcG+Fqej9+c1fi5bFxGo5BsOxOEA8JswWHXBzdyzJ+OHt9h2t/fm10E5tWTZ90fkav+3KLvydfga/E1sxk/l43LaCRMMCcg1AYcdq8Asxh/kicju7KGAkqlukrJvXorlV56R0II8gkL5O/y3/Df8jWkHDYdcZm4bEYkKQIGTB0W3vfVeDGIWsTFFyAxUhjV+JOUTtbSA8tupY3lOZwVyLsGJYnikUmKDbZTbOgUxQIDFA+NJM76U8J7Pky0ihy++eSoWTLjeO9c2DN+gO7reJImSwcNXW+cJ2CgswUMMfAZSQCEiwBPGBnd+JNUB5bRt1uvy+3AUA1h1/+7x16kYV+HKeqNRcAAewcM4/UaLQ9AEmn8ZoINjg2PR19hxj9uLuM3SFsbKuQ1mgcgxBMw0nJfIeHAtxq2K8dv6ekNPDe8jx7sesbwbn86L4AR4AkYbtLbqAKgmwiIyu9Xm1tdO2hd3SLNRYBd/rcG+ugXU7vk3mPerR0C9g0YcsXLyAKgiwiYKe7PRn1gFd2zbCN5XA7VhYANn1N8f9Sxx3BLfSaYDzBszovRjwXXdAeh1faRs2H+Zf9hWhm8mEYbp1QJC9jwlSW+zoOJDD+fpapM6QMai4ChE96M7gFo5gkYfb1fDeoCrbS58gJatyBxbFeuYpBMNOItvbyrT9TGHr28AA3fP2D4lHezCIDqImAl1z8X+EWd/J5Bfl1Xs6+aFpSWUYU7sXuQT+89PTlB7YFh5RgvPslHj8M8LB4KmGK/i5neDKRaOGDHI6TYoPlFncq7+ianP7PhRDkv2RKVQwHT7Hg123kARVdsMa/pAtYk2RdUOlLMVNvdzXggSFEVbIZUXyBGBFTYMGS6E6/MeiJQQRWNt8oADfuIKY+7M/ORYFK+lW6FhB+grRcQzX9aTCITn3VphTMBpdyMH6M/UN0LMP1x91Y5FFTK3qgY/YGqXoAl3nVhpVOB07picRx+DPIkQ5+RyEIvurGiZZzVOJj5B/l6AWlWBCz3slurDo0SujFAn7KvAJxx1WLkmJKUc7EAyKfzxOPcd6zm8p/1nNfc+0nLN2b/lxdBAEDe1P2k1/KepMsmbZlsSAgBQAhpkxAgXcPG0L9BGmJks/kjO66POSEEII3h2y5bzGXjRnciLABk8xUjF9of8wMwfPuCFLmZHSKCarA8ERg/PIB0uOENYNSHAACEBTB8hADgTBYYVgzMR4wsnsUHD0A/UpeH4BVgtIcAoINBCGD4CAEQHqDjwc2HBwAxmP43ClHFaA8PAJ0TE4fajfQwfngAhsWZ5mfMF2CkhwcAr0D5TKI60jKJkR4CYHW8KSECOvnMkMmL6kAIYLdwYbYIxG1g8NnCJwABQNxrAVGAhwMBABobUhxGDgrl/wUYADRKuSBRQkIYAAAAAElFTkSuQmCC