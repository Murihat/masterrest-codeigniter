<?php

defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . 'models/M_master.php';

class M_member extends M_master {

   	public function getAllMember(){
        return $this->getAll('t_member');
   	}

	public function getSingleMember($member_id){
		$where = array('member_id' => $member_id);
        return $this->getSingle('t_member', $where);
   	}

   	public function checkMemberByEmail($member_email){
		$where = array('member_email' => $member_email);
        return $this->getSingle('t_member', $where);
   	}

   	public function insertMember($data){
   		return $this->insert('t_member', $data);
   	}

   	public function updateMember($data, $where){
   		return $this->update('t_member', $data, $where);
   	}

   	public function deleteMember($where){
   		return $this->delete('t_member', $where);
   	}

}

?>