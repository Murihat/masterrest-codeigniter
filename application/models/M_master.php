<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class M_master extends CI_Model {

   	public function getAll($table){
	 	$this->db->select('*');
        return $this->db->get($table)->result();
   	}

	public function getSingle($table, $data){
	 	$this->db->select('*');
        return $this->db->get_where($table, $data)->row();
   	}

   	public function insert($table, $data){
   		return $this->db->insert($table, $data);
   	}

   	public function update($table, $data, $where){
		return $this->db->update($table, $data, $where);
   	}

   	public function delete($table, $where){
   		return $this->db->delete($table, $where);
   	}

}

?>