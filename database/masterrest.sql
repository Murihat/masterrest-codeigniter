-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Oct 07, 2019 at 06:46 AM
-- Server version: 10.1.38-MariaDB
-- PHP Version: 5.6.40

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `masterrest`
--

-- --------------------------------------------------------

--
-- Table structure for table `t_member`
--

CREATE TABLE `t_member` (
  `member_id` int(11) NOT NULL,
  `member_name` varchar(255) NOT NULL,
  `member_email` varchar(255) NOT NULL,
  `member_password` varchar(255) NOT NULL,
  `member_foto_url` varchar(255) DEFAULT NULL,
  `member_foto` varchar(255) DEFAULT NULL,
  `member_address` text,
  `member_phone` varchar(255) DEFAULT NULL,
  `member_status` int(11) NOT NULL,
  `member_type` int(11) DEFAULT NULL,
  `member_verification` int(11) DEFAULT NULL,
  `member_jenkel` varchar(255) DEFAULT NULL,
  `member_birthday` varchar(255) DEFAULT NULL,
  `member_registration_date` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `t_member`
--

INSERT INTO `t_member` (`member_id`, `member_name`, `member_email`, `member_password`, `member_foto_url`, `member_foto`, `member_address`, `member_phone`, `member_status`, `member_type`, `member_verification`, `member_jenkel`, `member_birthday`, `member_registration_date`) VALUES
(1, 'Muhamad Rizal Hidayat', 'Murihat@gmail.com', 'e10adc3949ba59abbe56e057f20f883e', 'https://www.tm-town.com/assets/', 'default_male300x300-aae6ae0235b6cd78cee8df7ae19f6085.png', NULL, NULL, 1, 1, 1, 'Pria', NULL, NULL),
(2, 'Nahrul Jinan', 'Jinan@gmail.com', 'e10adc3949ba59abbe56e057f20f883e', 'https://www.tm-town.com/assets/', 'default_male300x300-aae6ae0235b6cd78cee8df7ae19f6085.png', NULL, NULL, 1, 1, 1, 'Pria', NULL, NULL),
(3, 'Sutisna', 'Sutisna@gmail.com', 'e10adc3949ba59abbe56e057f20f883e', 'http://192.168.2.5/masterRest/assets/imgs/3/', '1570421576_3.png', 'LALALALA', '456789', 1, 1, 1, 'Pria', NULL, NULL),
(4, 'Dedy', 'Dedy@gmail.com', 'e10adc3949ba59abbe56e057f20f883e', 'https://www.tm-town.com/assets/', 'default_male300x300-aae6ae0235b6cd78cee8df7ae19f6085.png', NULL, NULL, 1, 1, 1, 'Pria', NULL, NULL),
(5, 'Jajang', 'jajang@gmail.com', 'e10adc3949ba59abbe56e057f20f883e', 'http://192.168.2.5/masterRest/assets/imgs/', '1570182120_.png', 'LALALALA', '456789', 1, 1, 1, 'Pria', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `t_member_status`
--

CREATE TABLE `t_member_status` (
  `status_id` int(11) NOT NULL,
  `member_status_id` int(11) NOT NULL,
  `member_status_name` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `t_member_status`
--

INSERT INTO `t_member_status` (`status_id`, `member_status_id`, `member_status_name`) VALUES
(1, 1, 'Active'),
(2, 0, 'Inactive'),
(3, 2, 'Dispensation'),
(4, 3, 'Blocked');

-- --------------------------------------------------------

--
-- Table structure for table `t_member_type`
--

CREATE TABLE `t_member_type` (
  `type_id` int(11) NOT NULL,
  `member_type_id` int(11) NOT NULL,
  `member_type_name` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `t_member_type`
--

INSERT INTO `t_member_type` (`type_id`, `member_type_id`, `member_type_name`) VALUES
(1, 1, 'Customer'),
(2, 2, 'Driver'),
(3, 3, 'Merchant');

-- --------------------------------------------------------

--
-- Table structure for table `t_member_verification`
--

CREATE TABLE `t_member_verification` (
  `verification_id` int(11) NOT NULL,
  `member_verification_id` int(11) NOT NULL,
  `member_verification_name` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `t_member_verification`
--

INSERT INTO `t_member_verification` (`verification_id`, `member_verification_id`, `member_verification_name`) VALUES
(1, 0, 'Not Verified'),
(2, 1, 'Verified');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `t_member`
--
ALTER TABLE `t_member`
  ADD PRIMARY KEY (`member_id`);

--
-- Indexes for table `t_member_status`
--
ALTER TABLE `t_member_status`
  ADD PRIMARY KEY (`status_id`);

--
-- Indexes for table `t_member_type`
--
ALTER TABLE `t_member_type`
  ADD PRIMARY KEY (`type_id`);

--
-- Indexes for table `t_member_verification`
--
ALTER TABLE `t_member_verification`
  ADD PRIMARY KEY (`verification_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `t_member`
--
ALTER TABLE `t_member`
  MODIFY `member_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `t_member_status`
--
ALTER TABLE `t_member_status`
  MODIFY `status_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `t_member_type`
--
ALTER TABLE `t_member_type`
  MODIFY `type_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `t_member_verification`
--
ALTER TABLE `t_member_verification`
  MODIFY `verification_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
